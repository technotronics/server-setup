#!/bin/bash

set +e
sudo mkdir -p /opt/certs
sudo docker rm -f nginx-proxy nginx-companion

set -e
sudo docker run -d -p 80:80 -p 443:443 \
  --name nginx-proxy \
  --restart always \
  -v /opt/certs:/etc/nginx/certs:ro \
  -v /etc/nginx/vhost.d \
  -v /usr/share/nginx/html \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  jwilder/nginx-proxy

sudo docker run -d \
  --name nginx-companion \
  --restart always \
  --volumes-from nginx-proxy \
  -v /opt/certs:/etc/nginx/certs:rw \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  alastaircoote/docker-letsencrypt-nginx-proxy-companion
